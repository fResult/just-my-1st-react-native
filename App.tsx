import React, { CSSProperties } from 'react'
import { Text, SafeAreaView, StyleSheet, View } from 'react-native'

function App(): React.ReactNode {
  return (
    <SafeAreaView style={styles['safe-area']}>
      <View style={styles.container}>
        <Text style={styles.text}>Here are some boxes of different colors</Text>
        <View style={[styles.box, styles.cyan]}>
          <Text style={styles['text-box']}>Cyan: #2aa198</Text>
        </View>
        <View style={[styles.box, styles.blue]}>
          <Text style={styles['text-box']}>Blue: #268bd2</Text>
        </View>
        <View style={[styles.box, styles.magenta]}>
          <Text style={styles['text-box']}>Magenta: #d33682</Text>
        </View>
        <View style={[styles.box, styles.orange]}>
          <Text style={styles['text-box']}>Orange: #cb4b16</Text>
        </View>
      </View>
    </SafeAreaView>
  )
}

type ClassSelectors =
  | 'container'
  | 'text'
  | 'text-box'
  | 'safe-area'
  | 'cyan-box'
  | 'box'
  | 'cyan'
  | 'blue'
  | 'magenta'
  | 'orange'

type ComponentStyles = StyleSheet.NamedStyles<{
  [classSelector in ClassSelectors]?: CSSProperties
}>

const styles = StyleSheet.create<ComponentStyles>({
  container: {
    paddingHorizontal: 10,
    paddingTop: 40,
    display: 'flex',
    rowGap: 10,
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  'text-box': {
    color: 'white',
    fontWeight: 'bold',
  },
  box: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cyan: {
    backgroundColor: '#2aa198',
  },
  blue: {
    backgroundColor: '#268bd2',
  },
  magenta: {
    backgroundColor: '#d33682',
  },
  orange: {
    backgroundColor: '#cb4b16',
  },
})

export default App
